package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private Controller controller;

    private Logger logger;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public View() {
        controller = new ControllerImp();
        logger = LogManager.getLogger(View.class);

        input = new Scanner(System.in);

        setMenu();

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1",this::pingPong);
        methodsMenu.put("2",this::fibonacci);
        methodsMenu.put("3",this::executorsFibonacci);
        methodsMenu.put("4",this::fibonacciCallable);
        methodsMenu.put("5",this::testScheduledThreadPool);
        methodsMenu.put("6",this::testSynchronized);
        methodsMenu.put("7",this::testPipe);
        methodsMenu.put("8",this::testLock);
        methodsMenu.put("9",this::testQueue);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - ping pong");
        menu.put("2", "2 - thread fibonacci");
        menu.put("3", "3 - executors fibonacci");
        menu.put("4", "4 - fibonacci callable");
        menu.put("5", "5 - test scheduled thread pool");
        menu.put("6", "6 - test synchronize");
        menu.put("7", "7 - test pipe");
        menu.put("8", "8 - test lock");
        menu.put("9", "9 - test queue");
    }

    private void Menu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            Menu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
                Thread.sleep(10000);
            } catch (Exception e) {
                logger.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

    private void pingPong(){
         logger.info(controller.pingPong());
    }

    private void fibonacci(){
        logger.info(controller.fibonacci());
    }

    private void executorsFibonacci(){
        controller.executorsFibonacci();
    }

    private void fibonacciCallable(){
        controller.fibonacciCallable();
    }

    private void testScheduledThreadPool(){
        try {
            controller.testScheduledThreadPool(input.nextInt());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void testSynchronized(){
        controller.testSynchronized();
    }

    private void testPipe(){
        controller.testPipe();
    }

    private void testLock(){
        controller.testLock();
    }

    private void testQueue(){
        controller.testQueue();
    }

}
