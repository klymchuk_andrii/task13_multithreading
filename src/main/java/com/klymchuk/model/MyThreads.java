package com.klymchuk.model;

import java.time.LocalDateTime;

public class MyThreads {
    private volatile static long count = 0;
    private static final Object object = new Object();

    public String pingPong() {
        StringBuilder stringBuilder = new StringBuilder();
        Thread t1 = new Thread(() -> {
            synchronized (object) {
                for (int i = 0; i < 100000; i++) {
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    count++;
                    object.notify();
                }

                stringBuilder.append("Finish: ").append(Thread.currentThread().getName()).append(" ").append(LocalDateTime.now());
            }

        });

        Thread t2 = new Thread(() -> {
            synchronized (object) {
                for (int i = 0; i < 100000; i++) {
                    object.notify();
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    count++;
                }
                stringBuilder.append("\nFinish: ").append(Thread.currentThread().getName()).append(" ").append(LocalDateTime.now());
            }
        });

        stringBuilder.append("Start: ").append(LocalDateTime.now()).append("\n");
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString() + "\nCount = " + count;
    }
}
