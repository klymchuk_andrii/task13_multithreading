package com.klymchuk.model;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class ThreadMethod {
    private ExecutorService pool = Executors.newCachedThreadPool();

    Future<Integer> runTask(int count) {
        return pool.submit(new FibonacciCallable(count));
    }

    void shutdown() {
        pool.shutdown();
    }
}