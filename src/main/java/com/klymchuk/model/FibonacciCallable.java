package com.klymchuk.model;

import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Integer> {
    private final int count;

    FibonacciCallable(int count) {
        this.count = count;
    }

    @Override
    public Integer call() throws Exception {
        int result = 0;
        for (int i = 0; i < count; i++) {
            result += fib(i);
        }
        return result;
    }

    private int fib(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }
}
