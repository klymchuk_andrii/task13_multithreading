package com.klymchuk.model;

public class Synchronized {
    private static final String string = new String("Hello");

    public void firstMethod() {
        new Thread(() -> {
            synchronized (string) {
                try {
                    System.out.println(string + " 1 thread");
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void secondMethod() {
        new Thread(() -> {
            synchronized (string) {
                try {
                    System.out.println(string + " 2 thread");
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void thirdMethod() {
        new Thread(() -> {
            synchronized (string) {
                try {
                    System.out.println(string + " 3 thread");
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
