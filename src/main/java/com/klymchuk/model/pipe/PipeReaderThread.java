package com.klymchuk.model.pipe;

import java.io.PipedReader;

public class PipeReaderThread implements Runnable{
    private PipedReader pr;
    private String name = null;

    public PipeReaderThread(String name, PipedReader pr) {
        this.name = name;
        this.pr = pr;
    }

    public void run() {
        try {
            while (true) {
                char c = (char) pr.read(); // read a char
                System.out.print(c);
            }
        } catch (Exception ignored) {
        }
    }
}
