package com.klymchuk.model;

import com.klymchuk.model.pipe.Pipe;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.*;

public class BusinessLogic implements Model {
    private MyThreads myThreads;

    @Override
    public String pingPong() {
        myThreads = new MyThreads();
        return myThreads.pingPong();
    }

    public String fibonacci() {
        Thread t = new Thread(new Fibonacci(20));
        try {
            t.start();
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "\nfibonacci";
    }

    public void executorsFibonacci() {
        ExecutorService pool = Executors.newCachedThreadPool();
        for (int i = 20; i < 25; i++) {
            pool.execute(new Fibonacci(i));
        }
        pool.shutdown();

        pool = Executors.newSingleThreadExecutor();
        for (int i = 35; i < 40; i++) {
            pool.execute(new Fibonacci(i));
        }
        pool.shutdown();

        pool = Executors.newFixedThreadPool(5);
        for (int i = 25; i < 30; i++) {
            pool.execute(new Fibonacci(i));
        }
        pool.shutdown();
    }

    public void fibonacciCallable() {
        ThreadMethod tm = new ThreadMethod();
        List<Future<Integer>> futures = new ArrayList<>();
        for (int i = 15; i < 20; i++) {
            futures.add(tm.runTask(i));
        }

        for (Future<Integer> f : futures) {
            try {
                System.out.println(f.get() + " ");
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            } finally {
                tm.shutdown();
            }
        }
    }

    public void testScheduledThreadPool(int quantity) throws InterruptedException {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(quantity);

        for (int i = 0; i < quantity; i++) {
            int sleepingTime = (int) ((Math.random() * 10) + 1);
            Thread thread = new Thread(() -> {
                System.out.println("Sleeping time of thread: " + sleepingTime);
            });

            scheduledThreadPool.schedule(thread, sleepingTime, TimeUnit.SECONDS);
        }
        scheduledThreadPool.shutdown();
    }

    @Override
    public void testSynchronized() {
        Synchronized s = new Synchronized();
        s.firstMethod();
        s.secondMethod();
        s.thirdMethod();
    }

    @Override
    public void testPipe() {
        new Pipe();
    }

    @Override
    public void testLock() {
        MyLock myLock = new MyLock();

        System.out.println("Start " + LocalDateTime.now());

        Thread thread1 = new Thread(myLock::firstMethod, "T1");
        Thread thread2 = new Thread(myLock::secondMethod, "T2");
        Thread thread3 = new Thread(myLock::thirdMethod, "T3");

        thread1.start();
        thread2.start();
        thread3.start();
    }

    @Override
    public void testQueue() {
        new MyQueue().testQueue();
    }
}
