package com.klymchuk.controller;

import com.klymchuk.model.BusinessLogic;
import com.klymchuk.model.Model;

public class ControllerImp implements Controller{
    private Model model;

    public ControllerImp(){
        model = new BusinessLogic();
    }

    @Override
    public String pingPong() {
        return model.pingPong();
    }

    @Override
    public String fibonacci() {
        return model.fibonacci();
    }

    @Override
    public void executorsFibonacci() {
        model.executorsFibonacci();
    }

    @Override
    public void fibonacciCallable() {
        model.fibonacciCallable();
    }

    @Override
    public void testScheduledThreadPool(int quantity) throws InterruptedException {
        model.testScheduledThreadPool(quantity);
    }

    @Override
    public void testSynchronized() {
        model.testSynchronized();
    }

    @Override
    public void testPipe() {
        model.testPipe();
    }

    @Override
    public void testLock() {
        model.testLock();
    }

    @Override
    public void testQueue() {
        model.testQueue();
    }
}
