package com.klymchuk.controller;

public interface Controller {
    String pingPong();
    String fibonacci();
    void executorsFibonacci();
    void fibonacciCallable();
    void testScheduledThreadPool(int quantity) throws InterruptedException;
    void testSynchronized();
    void testPipe();
    void testLock();
    void testQueue();
}
